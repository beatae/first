package Academy.Final;

import java.io.IOException;

import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;


import pageObjects.LoginPage;

import resources.baseClass;

public class homePage extends baseClass{
	
	public WebDriver driver;
	@BeforeTest
	public void startDriver() throws IOException{
		driver = initializeDriver();
		driver.manage().window().maximize();
		
	}
	
	@Test(dataProvider = "getData")
	public void basePageNavigation(String Username, String Password) throws IOException {
		
		  driver.get("http://automationpractice.com/index.php?controller=authentication&back=my-account");
		
		
			
		
			
			String current = driver.getCurrentUrl();
			String expected = "http://automationpractice.com/index.php?controller=authentication&back=my-account";
			
			Assert.assertEquals(current, expected);
			System.out.println("Automation login");
		
		 
		
		LoginPage lg= new LoginPage(driver);
		
		lg.getEmail().sendKeys(Username);
		lg.getPass().sendKeys(Password);
		lg.getLogin().click();
		
		

		
		
		String newcurrent = driver.getCurrentUrl();
		String logexpected = "http://automationpractice.com/index.php?controller=my-account";
		
		Assert.assertEquals(newcurrent, logexpected);
		System.out.println("Logged in success");
	
		
		
		
	}
	
	
	

	

	

	

	@DataProvider	
	public Object[][] getData(){
		Object[][] data = new Object[1][2];
		data[0][0] = "bromerodt@gmail.com";
		data[0][1]= "autoOk01";
		
		
		
		
		return data;
	}
	@AfterTest
	public void closedriver(){
	 
		driver.close();
	 
	}
	
	
	
}
